$(document).ready(function () {
	var idEmployee = $("body").data("id_user");

	getDiagram()

	function getDiagram() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/finalProjectAbsensiRestApi/diagram?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let diagramUsers = JSON.parse(response);
			var ctx = document.getElementById('myChart').getContext('2d');
			var myChart = new Chart(ctx, {
				type: 'pie',
				data: {
					labels: ['Masuk', 'Telat', 'mangkir'],
					datasets: [{
						data: [diagramUsers.data.masuk, diagramUsers.data.telat, diagramUsers.data.mangkir],
						backgroundColor: [
							'rgb(74, 221, 74)',
							'rgb(248, 248, 164)',
							'rgb(235, 116, 116)'
						]
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		});
	}
});

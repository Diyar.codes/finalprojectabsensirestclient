$(document).ready(function () {
	getDataAbsensi()

	function getDataAbsensi() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "Users",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsers, function (i, data) {
				$('#contentA').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.username}</td>
                    <td>${data.email}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-sm" id="dataAbsensi" data-toggle="modal" data-target="#basicExampleModalAbsensi" data-id=` + data.id_user + `>Absen</button>
                    </td>
                </tr>
            `);
			});
		});
	};

	$(document).on('click', '#dataAbsensi', function () {
		var form = new FormData();
		form.append("id_user", $(this).attr('data-id'));

		var settings = {
			"url": rest.url + "Users?id_user=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var user = JSON.parse(response);
			var dataUser = user.data;
			$('#absensiFormUser input[name = "id_user"]').val(dataUser.id_user);
			getDataAbsensiEmploye()
		});
	});

	function getDataAbsensiEmploye() {
		var form = new FormData();
		var id_user = $('#absensiFormUser input[name = "id_user"]').val()
		var settings = {
			"url": rest.url + "absensi?id_user=" + id_user,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsersAbsen = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsersAbsen, function (i, data) {
				$('#contentAD').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.username}</td>
					<td>${data.tgl}</td>
					<td>${data.bulan}</td>
					<td>${data.tahun}</td>
					<td>${data.ket}</td>
					<td>
						<i class="fas fa-user-edit btn-ubah mr-2 btn-ubahAbsensi" id="btn-ubahAbsensi" name="btn-ubahAbsensi" data-toggle="modal" data-target="#basicExampleModalUpdateAbsensi" data-id=` + data.id_absensi + `></i>
						<i class="fas fa-ban btn-hapus ml-2 btn-hapusAbsensi" data-id=` + data.id_absensi + ` id="btn-hapusAbsensi" name="btn-hapusAbsensi"></i> 
					</td>
                </tr>
            `);
			});
		});
	};

	$('#btnFormAbsensi').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Of course you want to add this timesheet ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("tgl", $('#insertFormAbsensi select[name = "tanggal"]').val());
				form.append("bulan", $('#insertFormAbsensi select[name = "bulan"]').val());
				form.append("tahun", $('#insertFormAbsensi select[name = "tahun"]').val());
				form.append("ket", $('#insertFormAbsensi select[name = "keterangan"]').val());
				form.append("id_user", $('#absensiFormUser input[name = "id_user"]').val());

				var settings = {
					"url": rest.url + "absensi",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'data added successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#insertFormAbsensi")[0].reset()
					$('.closee').click();
					getDataAbsensiEmploye();
				});
			}
		})
	})

	$('.close').click(function () {
		getDataAbsensi()
	})

	$(document).on('click', '#btn-hapusAbsensi', function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'Do you want to delete attendance data?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			if (result.value) {
				var settings = {
					"url": rest.url + "absensi",
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"data": {
						"id_absensi": $(this).attr('data-id')
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the Deleted!',
						'success'
					)
					getDataAbsensiEmploye()
				});
			}
		})
	})

	$(document).on('click', '#btn-ubahAbsensi', function () {
		var form = new FormData();
		form.append("id_absensi", $(this).attr('data-id'));

		var settings = {
			"url": rest.url + "absensi/getDataAbsensi?id_absensi=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var absensi = JSON.parse(response);
			var dataUser = absensi.data;
			$('#updateFormAbsensi select[name = "tanggal"]').val(dataUser.tgl);
			$('#updateFormAbsensi select[name = "bulan"]').val(dataUser.bulan);
			$('#updateFormAbsensi select[name = "tahun"]').val(dataUser.tahun);
			$('#updateFormAbsensi select[name = "keterangan"]').val(dataUser.ket);
			$('#updateFormAbsensi input[name = "id_user"]').val(dataUser.id_user);
			$('#updateFormAbsensi input[name = "id_absensi"]').val(dataUser.id_absensi);
		});
	});

	$('#btnFormAbsensii').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Of course you want to change this timesheet ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_absensi", $('#updateFormAbsensi input[name = "id_absensi"]').val());
				form.append("tgl", $('#updateFormAbsensi select[name = "tanggal"]').val());
				form.append("bulan", $('#updateFormAbsensi select[name = "bulan"]').val());
				form.append("tahun", $('#updateFormAbsensi select[name = "tahun"]').val());
				form.append("ket", $('#updateFormAbsensi select[name = "keterangan"]').val());
				form.append("id_user", $('#updateFormAbsensi input[name = "id_user"]').val());
				form.append("_method", "PUT");

				var settings = {
					"url": rest.url + "absensi/",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'data changed successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateFormAbsensi")[0].reset()
					$('.closee').click();
					getDataAbsensiEmploye();
				});
			}
		})
	})
})

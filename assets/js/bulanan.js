$(document).ready(function () {
	getDataUsers()

	function getDataUsers() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "Users",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsers, function (i, data) {
				$('#contentA').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.username}</td>
                    <td>${data.email}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-sm" id="dataBulanan" data-toggle="modal" data-target="#basicExampleModalBulanan" data-id=` + data.id_user + `>Absen</button>
                    </td>
                </tr>
            `);
			});
		});
	};

	$(document).on('click', '#dataBulanan', function (e) {
		e.preventDefault()
		var form = new FormData();
		form.append("id_user", $(this).attr('data-id'));

		var settings = {
			"url": rest.url + "Users?id_user=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
				$('.closeBulanan').click();
			}
		};

		$.ajax(settings).done(function (response) {
			var user = JSON.parse(response);
			var dataUser = user.data;
			$('#insertFormBulanan input[name = "id_user"]').val(dataUser.id_user);
		});
	})

	$('#btnFormBulanan').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Of course you ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Search Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_user", $('#insertFormBulanan input[name = "id_user"]').val());
				form.append("bulan", $('#insertFormBulanan select[name = "bulan"]').val());
				form.append("tahun", $('#insertFormBulanan select[name = "tahun"]').val());

				var settings = {
					"url": rest.url + "absensibulanan",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
						$('.closeBulan').click();
					}
				};

				$.ajax(settings).done(function (response) {
					let jsonUsers = JSON.parse(response);
					let jsonData = jsonUsers.data
					$(".remove").remove();
					$.each(jsonData, function (i, data) {
						$('#contentS').append(`
							<tr class="remove">
								<th scope="row">${i+1}</th>
								<td>${data.tgl}</td>
								<td>${data.bulan}</td>
								<td>${data.tahun}</td>
								<td>${data.ket}</td>
							</tr>
						`);
					});
					$('.closeBulanan').click();
				});
			}
		})
	})

	$('.closeBulan').click(function () {
		$("#insertFormBulanan")[0].reset()
		getDataUsers()
	})

	$('#btnFormGaji').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Of course you ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_user", $('#insertFormBulanan input[name = "id_user"]').val());
				form.append("bulan", $('#insertFormBulanan select[name = "bulan"]').val());
				form.append("tahun", $('#insertFormBulanan select[name = "tahun"]').val());

				var settings = {
					"url": rest.url + "gaji",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
						$('.closeBulan').click();
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the button!',
						'success'
					)
					$('.closeBulan').click();
				});
			}
		})
	})
})

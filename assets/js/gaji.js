$(document).ready(function () {
	getDataGaji()

	function getDataGaji() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "Users",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsers, function (i, data) {
				$('#contentG').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.username}</td>
                    <td>${data.email}</td>
                    <td>
                        <button type="button" class="btn btn-primary btn-sm" id="dataGaji" data-toggle="modal" data-target="#basicExampleModalGaji" data-id=` + data.id_user + `>Absen</button>
                    </td>
                </tr>
            `);
			});
		});
	};

	$(document).on('click', '#dataGaji', function (e) {
		e.preventDefault()
		var form = new FormData();
		form.append("id_user", $(this).attr('data-id'));

		var settings = {
			"url": rest.url + "Users?id_user=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var user = JSON.parse(response);
			var dataUser = user.data;
			$('#formGajiBulanan input[name = "id_user"]').val(dataUser.id_user);
			getDataGajiUsers()
		});
	})

	function getDataGajiUsers() {
		var form = new FormData();
		var id_user = $('#formGajiBulanan input[name = "id_user"]').val()
		var settings = {
			"url": rest.url + "gaji?id_user=" + id_user,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsersAbsen = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsersAbsen, function (i, data) {
				$('#contentGB').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.absen}</td>
					<td>${data.masuk}</td>
					<td>${data.telat}</td>
					<td>${data.mangkir}</td>
					<td>${data.bulan}</td>
					<td>${data.tahun}</td>
					<td>Rp. ${data.gaji_total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")},00</td>
					<td>
						<i class="fas fa-ban ml-2 btn-hapusGaji" data-id=` + data.id_gaji + ` id="btn-hapusGaji" name="btn-hapusGaji"></i>   
					</td>
                </tr>
            `);
			});
		});
	}

	$('.closeGaji').click(function () {
		getDataGaji()
	})

	$(document).on('click', '#btn-hapusGaji', function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'Do You Want To Remove Salary?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			if (result.value) {
				var settings = {
					"url": rest.url + "gaji",
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"data": {
						"id_gaji": $(this).attr('data-id')
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the Deleted!',
						'success'
					)
					getDataGajiUsers()
				});
			}
		})
	})
});

$(document).ready(function () {
	getCountUser()

	function getCountUser() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "users/countUser",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$('#contentCU').append(`
			    <div class="text-xs font-weight-bold text-primary text-uppercase mb-3">Total of all Employees</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">${jsonDataUsers} Employee</div>
			`)
		});
	};

	getCountComplain()

	function getCountComplain() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "complain/countComplain",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$('#contentCC').append(`
			    <div class="text-xs font-weight-bold text-uppercase text-danger mb-3">Total of all Complain</div>
                <div class="h5 mb-0 font-weight-bold text-danger text-danger-800">${jsonDataUsers} Complain</div>
			`)
		});
	};

	getDiagram()

	function getDiagram() {
		var form = new FormData();
		var settings = {
			"url": "http://localhost/finalProjectAbsensiRestApi/diagram/allemployee",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let diagramUsers = JSON.parse(response);
			var ctx = document.getElementById('myChart').getContext('2d');
			var myChart = new Chart(ctx, {
				type: 'pie',
				data: {
					labels: ['Masuk', 'Telat', 'mangkir'],
					datasets: [{
						data: [diagramUsers.data.masuk, diagramUsers.data.telat, diagramUsers.data.mangkir],
						backgroundColor: [
							'rgb(74, 221, 74)',
							'rgb(248, 248, 164)',
							'rgb(235, 116, 116)'
						]
					}]
				},
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
		});
	}

	getComplain()

	function getComplain() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "complain/userComplain",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsersComplain = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsersComplain, function (i, data) {
				$('#contentAC').append(`
					<div class="alert alert-danger remove" role="alert">
						<h6 class="alert-heading">${data.username} ~</h6>
						<p>${data.pesan}</p>
						<hr>
						<p class="mb-0">For Data : ${data.bulan}, ${data.tahun}</p>
					</div>
                `);
			});
		});
	}
})

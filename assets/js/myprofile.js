$(document).ready(function () {
	var idEmployee = $("body").data("id_user");

	getDataMyProfle()

	function getDataMyProfle() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "users/finance?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$(".remove").remove();
			$('#contentM').append(`
			<div class="card remove text-center">
				<img class="card-img-top" src="${rest.url}assets/img/user/${jsonDataUsers.image}" alt="Card image cap" width="100%" height="300px">
				<div class="card-body">
					<h4 class="card-title"><a>${jsonDataUsers.username}</a></h4>
					<p class="card-text">${jsonDataUsers.email}</p>
					<button type="button" class="btn btn-rounded btn-primary changeProfileAdmin" data-toggle="modal" data-target="#basicExamplechangeprofile" id="changeProfileAdmin" name="changeProfileAdmin" data-id=` + jsonDataUsers.id_user + `><i class="far fa-user pr-2" aria-hidden="true"></i>Change Profile</button>
					<button type="button" class="btn btn-rounded btn-brown changePasswordAdmin" data-toggle="modal" data-target="#basicExamplechangepassword" id="changePasswordAdmin" name="changePasswordAdmin" data-id=` + jsonDataUsers.id_user + `><i class="fas fa-redo pr-2" aria-hidden="true"></i>Change Password</button>
				</div>
            </div>
			`)
		});
	};

	$(document).on('click', '#changePasswordAdmin', function () {
		var form = new FormData();

		var settings = {
			"url": rest.url + "users/finance?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var profile = JSON.parse(response);
			var myprofile = profile.data;
			$('#updateFormChangePassword input[name = "id_user"]').val(myprofile.id_user);
		});
	});

	$('#btnFormChangePassword').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'do you want to change the Password ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, change Password!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_user", $('#updateFormChangePassword input[name = "id_user"]').val());
				form.append("current_password", $('#updateFormChangePassword input[name = "current_password"]').val());
				form.append("_method", "PUT");
				form.append("new_password1", $('#updateFormChangePassword input[name = "new_password1"]').val());
				form.append("new_password2", $('#updateFormChangePassword input[name = "new_password2"]').val());

				var settings = {
					"url": rest.url + "users/changepasswordfinance",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'data Password Change successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateFormChangePassword")[0].reset()
					$('.closePassword').click();
					getDataMyProfle()
				});
			}
		})
	})

	$(document).on('click', '#changeProfileAdmin', function () {
		var form = new FormData();

		var settings = {
			"url": rest.url + "users/finance?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var profile = JSON.parse(response);
			var myprofile = profile.data;

			$('#updateFormChangeProfile input[name = "id_user"]').val(myprofile.id_user);
			$('#updateFormChangeProfile input[name = "username"]').val(myprofile.username);
			$('#updateFormChangeProfile input[name = "email"]').val(myprofile.email);
			$('.gambarLama').html(
				`<img src="${rest.url}assets/img/user/${myprofile.image}" width="100%" height="300" id="gambarBaru">`
			);
		});
	});

	$('#btnFormChangeProfile').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'do you want to change the Profile ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, change Profile!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				var file = $('#updateFormChangeProfile input[name = "image"]')[0].files[0];
				form.append("id_user", $('#updateFormChangeProfile input[name = "id_user"]').val());
				form.append("_method", "PUT");
				form.append("username", $('#updateFormChangeProfile input[name = "username"]').val());
				form.append("email", $('#updateFormChangeProfile input[name = "email"]').val());
				form.append("image", file);

				var settings = {
					"url": rest.url + "users/changeProfileFinance",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'data Profile Change successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateFormChangeProfile")[0].reset()
					$('.closeProfil').click();
					getDataMyProfle()
				});
			}
		})
	})
});


function previewFile(input) {
	var file = $("#image").get(0).files[0];

	if (file) {
		var reader = new FileReader();

		reader.onload = function () {
			$("#gambarBaru").attr("src", reader.result);
		}

		reader.readAsDataURL(file);
	}
}

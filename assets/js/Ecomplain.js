$(document).ready(function () {
	var idEmployee = $("body").data("id_user");

	getEcomplain()

	function getEcomplain() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "complain?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				if (err.status == false) {
					$(".remove").remove();
				}
			}
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsersComplain = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsersComplain, function (i, data) {
				$('#contentC').append(`
				<div class="alert alert-danger remove" role="alert">
					<h6 class="alert-heading">${data.username} ~</h6>
					<p>${data.pesan}</p>
					<hr>
					<p class="mb-0">For Data : ${data.bulan}, ${data.tahun}</p>
					<button type="button" class="btn purple-gradient btn-sm btn-ubahComplain" id="btn-ubahComplain" name="btn-ubahComplain" data-toggle="modal" data-target="#basicExampleModalUpdateComplain" data-id=` + data.id_complain + `>Ubah</button>
					<button class="btn peach-gradient btnDeleteComplain" id="btnDeleteComplain" name="btnDeleteComplain"  data-id=` + data.id_complain + `>Delete</button>
            	</div>
                `);
			});
		});
	}

	$(document).on('click', '#btnDeleteComplain', function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'sure you want to delete the complain data?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			if (result.value) {
				var settings = {
					"url": rest.url + "complain",
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"data": {
						"id_complain": $(this).attr('data-id')
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the Deleted!',
						'success'
					)
					getEcomplain()
				});
			}
		})
	})

	$(document).on('click', '#btn-ubahComplain', function () {
		var form = new FormData();
		form.append("id_complain", $(this).attr('data-id'));

		var settings = {
			"url": rest.url + "complain?id_complain=" + $(this).attr('data-id') + "&id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var absensi = JSON.parse(response);
			var complain = absensi.data;
			$('#updateFormComplain input[name = "id_user"]').val(complain[0].id_user);
			$('#updateFormComplain input[name = "id_complain"]').val(complain[0].id_complain);
			$('#updateFormComplain input[name = "id_gaji"]').val(complain[0].id_gaji);
			$('#updateFormComplain textarea[name = "pesan"]').val(complain[0].pesan);
		});
	});

	$('#btnFormComplainUpdate').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'do you want to change the content ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, chnage Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_complain", $('#updateFormComplain input[name = "id_complain"]').val());
				form.append("pesan", $('#updateFormComplain textarea[name = "pesan"]').val());
				form.append("_method", "PUT");
				form.append("id_user", $('#updateFormComplain input[name = "id_user"]').val());
				form.append("id_gaji", $('#updateFormComplain input[name = "id_gaji"]').val());

				var settings = {
					"url": rest.url + "complain",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'data changed successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateFormComplain")[0].reset()
					$('.closeComplain').click();
					getEcomplain()
				});
			}
		})
	})
});

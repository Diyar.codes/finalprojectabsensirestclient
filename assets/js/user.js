$(document).ready(function () {
	getDataUsers()

	function getDataUsers() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "Users",
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsers = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsers, function (i, data) {
				$('#contentU').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.username}</td>
                    <td>${data.email}</td>
                    <td>${data.level}</td>
					<td>
							<i class="fas fa-user-edit btn-ubah mr-2" id="btn-ubah" name="btn-ubah" data-toggle="modal" data-target="#basicExampleModalUpdateUser" data-id=` + data.id_user + ` ></i>
							<i class="fas fa-ban btn-hapus ml-2" data-id=` + data.id_user + ` id="btn-hapus" name="btn-hapus"></i>
					</td>
                </tr>
            `);
			});
		});
	};

	$('#btnFormUser').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Sure you want to add this item?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Upload Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("username", $('#insertFormUser input[name = "username"]').val());
				form.append("email", $('#insertFormUser input[name = "email"]').val());
				form.append("password", $('#insertFormUser input[name = "password"]').val());
				form.append("passwordConfirm", $('#insertFormUser input[name = "passwordConfirm"]').val());

				var settings = {
					"url": rest.url + "Users",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Data added successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#insertFormUser")[0].reset()
					$('.close').click();
					getDataUsers()
				});
			}
		})
	})

	$(document).on('click', '#btn-hapus', function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'Do You Want To Remove Users?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya'
		}).then((result) => {
			if (result.value) {
				var settings = {
					"url": rest.url + "Users",
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"data": {
						"id_user": $(this).attr('data-id')
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the Deleted!',
						'success'
					)
					getDataUsers()
				});
			}
		})
	})

	$(document).on('click', '#btn-ubah', function () {
		var form = new FormData();
		form.append("id_user", $(this).attr('data-id'));

		var settings = {
			"url": rest.url + "Users?id_user=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var user = JSON.parse(response);
			var dataUser = user.data;
			$('#updateFormUser input[name = "username"]').val(dataUser.username);
			$('#updateFormUser input[name = "email"]').val(dataUser.email);
			$('#updateFormUser input[name = "id_user"]').val(dataUser.id_user);
		});
	});

	$('#btnFormUserr').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Sure you want to change this item?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, change Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("_method", "PUT");
				form.append("id_user", $('#updateFormUser input[name = "id_user"]').val());
				form.append("username", $('#updateFormUser input[name = "username"]').val());
				form.append("email", $('#updateFormUser input[name = "email"]').val());

				var settings = {
					"url": rest.url + "Users",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Data changed successfully',
						showConfirmButton: false,
						timer: 1000
					})
					$("#updateFormUser")[0].reset()
					$('.close').click();
					getDataUsers()
				});
			}
		})
	})
})

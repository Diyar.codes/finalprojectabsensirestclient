$(document).ready(function () {
	var idEmployee = $("body").data("id_user");

	getDataGajiEmploye()

	function getDataGajiEmploye() {
		var form = new FormData();
		var settings = {
			"url": rest.url + "gaji?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			let jsonUsers = JSON.parse(response);
			let jsonDataUsersAbsen = jsonUsers.data;
			$(".remove").remove();
			$.each(jsonDataUsersAbsen, function (i, data) {
				$('#contentEN').append(`
                <tr class="remove">
                    <th scope="row">${i+1}</th>
                    <td>${data.absen}</td>
					<td>${data.masuk}</td>
					<td>${data.telat}</td>
					<td>${data.mangkir}</td>
					<td>${data.bulan}</td>
					<td>${data.tahun}</td
					<td>${data.gaji_total}</td>
					<td>Rp. ${data.gaji_total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")},00</td>
					<td> 
						<i class="fas fa-exclamation-triangle btn-hapus ml-2 btncomplain" id="btncomplain" name="btncomplain" data-toggle="modal" data-target="#complain" data-id=` + data.id_gaji + `></i> 
					</td>
                </tr>
            `);
			});
		});
	}

	$(document).on('click', '#dataBulan', function (e) {
		e.preventDefault()
		var form = new FormData();

		var settings = {
			"url": rest.url + "Users?id_user=" + idEmployee,
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
				$('.closeBulanan').click();
			}
		};

		$.ajax(settings).done(function (response) {
			var user = JSON.parse(response);
			var dataUser = user.data;
			$('#insertFormBulan input[name = "id_user"]').val(dataUser.id_user);
		});
	})

	$('#btnFormBulan').click(function (e) {
		e.preventDefault();
		Swal.fire({
			title: 'Of course you ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Search Data!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_user", $('#insertFormBulan input[name = "id_user"]').val());
				form.append("bulan", $('#insertFormBulan select[name = "bulan"]').val());
				form.append("tahun", $('#insertFormBulan select[name = "tahun"]').val());

				var settings = {
					"url": rest.url + "absensibulanan",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
						$('.closeBulan').click();
					}
				};

				$.ajax(settings).done(function (response) {
					let jsonUsers = JSON.parse(response);
					let jsonData = jsonUsers.data
					$.each(jsonData, function (i, data) {
						$('#contentS').append(`
							<tr>
								<th scope="row">${i+1}</th>
								<td>${data.tgl}</td>
								<td>${data.bulan}</td>
								<td>${data.tahun}</td>
								<td>${data.ket}</td>
							</tr>
						`);
					});
					$("#insertFormBulan")[0].reset()
					$('.closeBulanan').click();
				});
			}
		})
	})

	$('.container-fluid').click(function () {
		getDataGajiEmploye()
	})

	$('.closeBulan').click(function () {
		$("#contentS").html('')
	})

	$(document).on('click', '#btncomplain', function (e) {
		e.preventDefault()
		var form = new FormData();

		var settings = {
			"url": rest.url + "gaji?id_user=" + idEmployee + "&id_gaji=" + $(this).attr('data-id'),
			"method": "GET",
			"timeout": 0,
			"headers": {
				"X-Token": rest.token
			},
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form
		};

		$.ajax(settings).done(function (response) {
			var user = JSON.parse(response);
			var dataUser = user.data;
			$('#FormAbsensiBulananComplain input[name = "id_user"]').val(dataUser[0].id_user);
			$('#FormAbsensiBulananComplain input[name = "id_gaji"]').val(dataUser[0].id_gaji);
			$('#FormAbsensiBulananComplain input[name = "username"]').val(dataUser[0].username);
		});
	})

	$('#btnFormComplain').click(function (e) {
		e.preventDefault()
		Swal.fire({
			title: 'Of course you ? ',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Send Complain!'
		}).then((result) => {
			if (result.value) {
				var form = new FormData();
				form.append("id_user", $('#FormAbsensiBulananComplain input[name = "id_user"]').val());
				form.append("pesan", $('#FormAbsensiBulananComplain textarea[name = "pesan"]').val());
				form.append("id_gaji", $('#FormAbsensiBulananComplain input[name = "id_gaji"]').val());

				var settings = {
					"url": rest.url + "complain",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"X-Token": rest.token
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form,
					error: function (jqXHR, textStatus, errorThrown) {
						var err = JSON.parse(jqXHR.responseText)
						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: [err.message]
						})
						$('.closeComplain').click();
					}
				};

				$.ajax(settings).done(function (response) {
					Swal.fire(
						'Good job!',
						'You clicked the button!',
						'success'
					)
					$("#FormAbsensiBulananComplain")[0].reset()
					$('.closeComplain').click();
				});
			}
		})
	})
});

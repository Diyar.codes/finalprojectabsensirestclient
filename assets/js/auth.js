$(document).ready(function () {
	$('#btnLogin').click(function (e) {
		e.preventDefault()
		var form = new FormData();
		form.append("email", $('#formLogin input[name = "email"]').val());
		form.append("password", $('#formLogin input[name = "password"]').val());

		var settings = {
			"url": rest.url + "auth",
			"method": "POST",
			"timeout": 0,
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": form,
			error: function (jqXHR, textStatus, errorThrown) {
				var err = JSON.parse(jqXHR.responseText)
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: [err.message]
				})
			}
		};

		$.ajax(settings).done(function (response) {
			var dataLogin = JSON.parse(response);
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				timerProgressBar: true,
				onOpen: (toast) => {
					toast.addEventListener('mouseenter', Swal.stopTimer)
					toast.addEventListener('mouseleave', Swal.resumeTimer)
				}
			})

			Toast.fire({
				icon: 'success',
				title: 'Login in successfully'
			});

			setTimeout(function () {
				window.location.href = "auth/confirmLogin/" +
					dataLogin.data.id_user + "/" +
					dataLogin.data.username + "/" +
					dataLogin.data.level;
			}, 3000);
		});
	})

	$('#showHide').click(function () {
		var input = $("#password");
		if (input.attr("type") === "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	})
})

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function index()
    {
        $data['title'] = 'Login';

        if ($this->session->userdata('level') == 'karyawan') {
            redirect(base_url('Auth/block'));
        } else if ($this->session->userdata('level') == 'finance') {
            redirect(base_url('Auth/block'));
        }

        $this->load->view('templates/auth/header', $data);
        $this->load->view('auth/login');
        $this->load->view('templates/auth/footer');
    }

    function confirmLogin($id_user, $username, $level)
    {
        if ($id_user == "" || $username == "" || $level  == "") {
            redirect(base_url('auth'));
        } else {
            if ($level == "finance") {
                $this->session->set_userdata([
                    'id_user' => $id_user,
                    'username' => $username,
                    'level' => $level
                ]);
                redirect(base_url('Home/index'));
            } else {
                $this->session->set_userdata([
                    'id_user' => $id_user,
                    'username' => $username,
                    'level' => $level
                ]);
                redirect(base_url('Employees/index'));
            }
        }
    }

    function logout()
    {
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('level');

        redirect(base_url('Auth'));
    }

    function block()
    {
        $this->load->view('auth/block');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employees extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('level') != 'karyawan') {
            redirect(base_url('Auth/block'));
        }
    }

    function index()
    {
        $data['title'] = 'Home';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/sidebar');
        $this->load->view('templates/user/navbar');
        $this->load->view('user/home/index');
        $this->load->view('templates/user/footer');
        $this->load->view('templates/user/footerjquerycountall');
    }

    function gajiBulanan()
    {
        $data['title'] = 'Sallary Moon';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/sidebar');
        $this->load->view('templates/user/navbar');
        $this->load->view('user/bulanan/index');
        $this->load->view('templates/user/footer');
        $this->load->view('templates/user/footerjquerybulanan');
    }

    function complain()
    {
        $data['title'] = 'Complain';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/sidebar');
        $this->load->view('templates/user/navbar');
        $this->load->view('user/complain/index');
        $this->load->view('templates/user/footer');
        $this->load->view('templates/user/footerjquerycomplain');
    }

    function myProfile()
    {
        $data['title'] = 'My Profile';

        $this->load->view('templates/user/header', $data);
        $this->load->view('templates/user/sidebar');
        $this->load->view('templates/user/navbar');
        $this->load->view('user/my_profile/index');
        $this->load->view('templates/user/footer');
        $this->load->view('templates/user/footerjquerymyprofile');
    }
}

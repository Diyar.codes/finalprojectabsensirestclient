<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('level') != 'finance') {
            redirect(base_url('Auth/block'));
        }
    }

    function index()
    {
        $data['title'] = 'Home';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/home/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjquerycountall');
    }

    function user()
    {
        $data['title'] = 'User';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/user/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjqueryuser');
    }

    function absensi()
    {
        $data['title'] = 'Absensi';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/absensi/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjqueryabsensi');
    }

    function bulanan()
    {
        $data['title'] = 'Bulanan';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/bulanan/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjquerybulanan');
    }

    function gaji()
    {
        $data['title'] = 'Gaji Bulanan';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/gaji/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjquerygaji');
    }

    function complain()
    {
        $data['title'] = 'Complain';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/complain/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjquerycomplain');
    }

    function myProfile()
    {
        $data['title'] = 'My Profile';

        $this->load->view('templates/admin/header', $data);
        $this->load->view('templates/admin/sidebar');
        $this->load->view('templates/admin/navbar');
        $this->load->view('admin/my_profile/index');
        $this->load->view('templates/admin/footer');
        $this->load->view('templates/admin/footerjquerymyprofile');
    }
}

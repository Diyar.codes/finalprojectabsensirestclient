<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">My Profile</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-10 col-12" id="contentM">
        </div>

        <div class="modal fade" id="basicExamplechangepassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                        <button type="button" class="closePassword" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="updateFormChangePassword">
                            <input type="hidden" name="id_user">
                            <div class="form-group">
                                <input type="password" class="form-control current_password" id="current_password" name="current_password" placeholder="Current Password">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="password" class="form-control new_password1" id="new_password1" name="new_password1" placeholder="New Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="password" class="form-control new_Password2" id="new_password2" name="new_password2" placeholder="Confirm New Password">
                                </div>
                            </div>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormChangePassword" id="btnFormChangePassword" name="btnFormChangePassword"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Change Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="basicExamplechangeprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Profile</h5>
                        <button type="button" class="closeProfil" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" class="md-form" method="post" id="updateFormChangeProfile">
                            <input type="hidden" name="id_user">
                            <div class="form-group">
                                <input type="text" class="form-control username" id="username" name="username" placeholder="Username..">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control email" id="email" name="email" placeholder="Email..">
                            </div>
                            <div class="form-group">
                                <input type="file" onchange="previewFile(this);" class="form-control image" id="image" name="image">
                                <div class="gambarLama">
                                </div>
                            </div>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormChangeProfile" id="btnFormChangeProfile" name="btnFormChangeProfile"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Change Profil</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
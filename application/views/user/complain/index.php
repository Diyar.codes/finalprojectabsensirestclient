<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Complain</h1>
    </div>
    <div class="row">
        <div class="col-lg-12" id="contentC">

        </div>

        <div class="modal fade" id="basicExampleModalUpdateComplain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Absensi</h5>
                        <button type="button" class="closeComplain" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="updateFormComplain">
                            <div class="col-md-4">
                                <input type="hidden" name="id_user">
                                <input type=hidden name="id_complain">
                                <input type="hidden" name="id_gaji">
                            </div>
                            <div class="md-form">
                                <textarea id="textarea-char-counter pesan" class="form-control md-textarea pesan" name="pesan" length="120" rows="3"></textarea>
                            </div>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormComplainUpdate" id="btnFormComplainUpdate" name="btnFormComplainUpdate"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Change</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
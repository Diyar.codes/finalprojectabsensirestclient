<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Gaji Bulanan</h1>
    </div>
    <div class="row">
        <div class="col-xl-10">
            <div class="table-responsive text-nowrap">
                <table class="table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">absen</th>
                            <th scope="col">masuk</th>
                            <th scope="col">telat</th>
                            <th scope="col">mangkir</th>
                            <th scope="col">bulan</th>
                            <th scope="col">tahun</th>
                            <th scope="col">gaji total</th>
                        </tr>
                    </thead>
                    <tbody id="contentEN">
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xl-2">
            <button type="button" class="btn btn-primary btn-lg btn-block" id="dataBulan" data-toggle="modal" data-target="#basicExampleModalBulan"><i class="fas fa-arrows-alt mr-1" aria-hidden="true"></i>Bulanan</button>
        </div>

        <div class="modal fade" id="basicExampleModalBulan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title w-100" id="myModalLabel">Absensi Bulanan</h4>
                        <button type="button" class="closeBulanan" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="insertFormBulan">
                            <div class="row">
                                <input type="hidden" name="id_user">
                                <div class="col-6">
                                    <select class="browser-default custom-select" name="bulan">
                                        <option value="" selected>bulan</option>
                                        <option value="januari">Januari</option>
                                        <option value="februari">Februari</option>
                                        <option value="maret">Maret</option>
                                        <option value="april">April</option>
                                        <option value="mei">Mei</option>
                                        <option value="juni">Juni</option>
                                        <option value="juli">Juli</option>
                                        <option value="agustus">agustus</option>
                                        <option value="september">september</option>
                                        <option value="oktober">oktober</option>
                                        <option value="november">november</option>
                                        <option value="desember">desember</option>
                                    </select>
                                </div>
                                <div class="col-6">
                                    <select class="browser-default custom-select" name="tahun">
                                        <option value="" selected>Tahun</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                            </div>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormBulan" id="btnFormBulan" name="btnFormBulan" data-toggle="modal" data-target="#basicExampleModalInsertBulanan"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Filter</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="basicExampleModalInsertBulanan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bulanan</h5>
                        <button type="button" class="closeBulan" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="FormAbsensiBulanan">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">tgl</th>
                                        <th scope="col">bulan</th>
                                        <th scope="col">tahun</th>
                                        <th scope="col">Ket</th>
                                    </tr>
                                </thead>
                                <tbody id="contentS">
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="complain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Complain</h5>
                        <button type="button" class="closeComplain" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="FormAbsensiBulananComplain">
                            <input type="hidden" name="id_user">
                            <input type="hidden" name="id_gaji">
                            <input type="hidden" name="username">
                            <input type="hidden" name="bulan">
                            <input type="hidden" name="tahun">
                            <div class="md-form">
                                <textarea id="textarea-char-counter pesan" class="form-control md-textarea pesan" name="pesan" length="120" rows="3"></textarea>
                                <label for="textarea-char-counter">input your Complain</label>
                            </div>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormComplain" id="btnFormComplain" name="btnFormComplain"><i class="fas fa-plane pr-2" aria-hidden="true"></i>SEND</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
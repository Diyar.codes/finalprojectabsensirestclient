<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Employee ~ </h1>
    </div>

    <div class="row d-flex justify-content-center justify-content-lg-start">
        <div class="col-xl-4 col-lg-5 col-md-8 col-sm-10 col-12 text-center">
            <button type="button" class="btn blue-gradient pl-5 pr-5 btn-block" data-toggle="modal" data-target="#basicExampleModalInsertUser">
                <i class="fas fa-user-plus mr-3"></i>employee
            </button>
        </div>

        <div class="modal fade" id="basicExampleModalInsertUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ADD Employee</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="insertFormUser">
                            <div class="form-group">
                                <input type="text" class="form-control username" name="username" id="username" autocomplete="off" placeholder="Username...">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control email" name="email" id="email" autocomplete="off" placeholder="Email...">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="password" class="form-control password" name="password" id="password" autocomplete="off" placeholder="Password...">
                                    </div>
                                    <div class="col-6">
                                        <input type="password" class="form-control passwordConfirm" name="passwordConfirm" id="passwordConfirm" autocomplete="off" placeholder="Password Confirm...">
                                    </div>
                                </div>
                            </div>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormUser" id="btnFormUser" name="btnFormUser"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade basicExampleModalUpdateUser" id="basicExampleModalUpdateUser" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update USER</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="updateFormUser">
                            <input type="hidden" name="id_user">
                            <div class="form-group">
                                <input type="text" class="form-control username" name="username" id="username" autocomplete="off" placeholder="Username...">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control email" name="email" id="email" autocomplete="off" placeholder="Email...">
                            </div>
                            <div class="btnModal-lg">
                                <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormUserr" id="btnFormUserr" name="btnFormUserr"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive text-nowrap">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Level</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody id="contentU">
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
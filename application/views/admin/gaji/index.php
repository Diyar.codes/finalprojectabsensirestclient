<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Gaji Bulanan ~ </h1>
    </div>
    <div class="row">
        <div class="modal fade" id="basicExampleModalGaji" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title w-100" id="myModalLabel">Gaji Bulanan</h4>
                        <button type="button" class="closeGaji" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="formGajiBulanan">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-hover text-center">
                                    <input type="hidden" name="id_user">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">absen</th>
                                            <th scope="col">masuk</th>
                                            <th scope="col">telat</th>
                                            <th scope="col">mangkir</th>
                                            <th scope="col">bulan</th>
                                            <th scope="col">tahun</th>
                                            <th scope="col">gaji total</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contentGB">
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive text-nowrap">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Data Gaji</th>
                    </tr>
                </thead>
                <tbody id="contentG">
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
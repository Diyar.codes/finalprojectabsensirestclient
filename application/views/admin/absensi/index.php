<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Absensi ~ </h1>
    </div>
    <div class="row">
        <div class="modal fade" id="basicExampleModalAbsensi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-fluid" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title w-100" id="myModalLabel">Absensi</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row d-flex justify-content-center justify-content-lg-start">
                            <div class="col-xl-4 col-lg-5 col-md-8 col-sm-10 col-12 text-center">
                                <button type="button" class="btn blue-gradient pl-5 pr-5 btn-block" data-toggle="modal" data-target="#basicExampleModalInsertAbsensi"><i class="fas fa-clipboard-list mr-3"></i>add absensi</button>
                            </div>
                        </div>
                        <form action="" method="post" id="absensiFormUser">
                            <div class="table-responsive text-nowrap">
                                <table class="table table-hover text-center">
                                    <input type="hidden" name="id_user">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">username</th>
                                            <th scope="col">tgl</th>
                                            <th scope="col">bulan</th>
                                            <th scope="col">tahun</th>
                                            <th scope="col">ket</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contentAD">
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="basicExampleModalInsertAbsensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Absensi</h5>
                        <button type="button" class="closee" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="insertFormAbsensi">
                            <div class="row">
                                <div class="col-4">
                                    <select class="browser-default custom-select" name="tanggal">
                                        <option value="" selected>tanggal</option>
                                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                                            <option value="<?= $i; ?>"><?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="browser-default custom-select" name="bulan">
                                        <option value="" selected>bulan</option>
                                        <option value="januari">Januari</option>
                                        <option value="februari">Februari</option>
                                        <option value="maret">Maret</option>
                                        <option value="april">April</option>
                                        <option value="mei">Mei</option>
                                        <option value="juni">Juni</option>
                                        <option value="juli">Juli</option>
                                        <option value="agustus">agustus</option>
                                        <option value="september">september</option>
                                        <option value="oktober">oktober</option>
                                        <option value="november">november</option>
                                        <option value="desember">desember</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="browser-default custom-select" name="tahun">
                                        <option value="" selected>Tahun</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                            </div>
                            <select class="browser-default custom-select mt-2" name="keterangan">
                                <option value="" selected>Keterangan</option>
                                <option value="masuk">Masuk</option>
                                <option value="telat">Telat</option>
                                <option value="mangkir">Mangkir</option>
                            </select>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormAbsensi" id="btnFormAbsensi" name="btnFormAbsensi"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="basicExampleModalUpdateAbsensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Absensi</h5>
                        <button type="button" class="closee" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="updateFormAbsensi">
                            <div class="row">
                                <div class="col-4">
                                    <input type="hidden" name="id_user">
                                    <input type="hidden" name="id_absensi">
                                    <select class="browser-default custom-select" name="tanggal">
                                        <option value="" selected>tanggal</option>
                                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                                            <option value="<?= $i; ?>"><?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="browser-default custom-select" name="bulan">
                                        <option value="" selected>bulan</option>
                                        <option value="januari">Januari</option>
                                        <option value="februari">Februari</option>
                                        <option value="maret">Maret</option>
                                        <option value="april">April</option>
                                        <option value="mei">Mei</option>
                                        <option value="juni">Juni</option>
                                        <option value="juli">Juli</option>
                                        <option value="agustus">agustus</option>
                                        <option value="september">september</option>
                                        <option value="oktober">oktober</option>
                                        <option value="november">november</option>
                                        <option value="desember">desember</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <select class="browser-default custom-select" name="tahun">
                                        <option value="" selected>Tahun</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                    </select>
                                </div>
                            </div>
                            <select class="browser-default custom-select mt-2" name="keterangan">
                                <option value="" selected>Keterangan</option>
                                <option value="masuk">Masuk</option>
                                <option value="telat">Telat</option>
                                <option value="mangkir">Mangkir</option>
                            </select>
                            <button class="btn blue-gradient btn-block pl-5 pr-5 mt-3 mb-3 btnFormAbsensii" id="btnFormAbsensii" name="btnFormAbsensii"><i class="fas fa-plane pr-2" aria-hidden="true"></i>Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive text-nowrap">
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">List Absen</th>
                    </tr>
                </thead>
                <tbody id="contentA">
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard ~ </h1>
    </div>
    <div class="row">
        <div class="col-xl-6 col-md-6 mb-4 cardLink">
            <a href="<?= base_url('home/user'); ?>">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2" id="contentCU">

                            </div>
                            <div class="col-auto">
                                <i class="fas fa-users fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-xl-6 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2" id="contentCC">
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-exclamation-triangle fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row mt-3 mb-3">
        <div class="col-lg-6">
            <h6>diagrams all employee career ~ </h6>
            <canvas id="myChart" width="200" height="200"></canvas>
        </div>

        <div class="col-lg-6 complainAdmin">
            <h6>Complain employee ~ </h6>
            <div id="contentAC" class="scrollbar-light-blue"></div>
        </div>
    </div>
    <hr>
</div>
</div>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('Home'); ?>">
        <div class="sidebar-brand-icon">
            <i class="fas fa-building"></i>
        </div>
        <div class="sidebar-brand-text mx-3">MJP Office</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'index') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Home/index') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'user') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Home/user') ?>">
            <i class="fas fa-users"></i>
            <span>Users</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'absensi') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Home/absensi') ?>">
            <i class="fas fa-list"></i>
            <span>Absensi</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'bulanan') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Home/bulanan') ?>">
            <i class="fas fa-list-alt"></i>
            <span>Bulanan</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'gaji') {
                            echo "active";
                        } ?>">
        <a class=" nav-link" href="<?= base_url('Home/gaji') ?>">
            <i class="fas fa-hand-holding-usd"></i>
            <span>Gaji</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= $title; ?></title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="<?= base_url('assets/MDB-Free.4.19.1/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/MDB-Free.4.19.1/css/mdb.min.css') ?>">

    <link href="<?= base_url('assets/SB ADMIN2/') ?>css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body id="page-top" data-id_user="<?= $this->session->userdata('id_user'); ?>">
    <div id="wrapper">
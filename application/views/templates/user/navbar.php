<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <div class="container">
                <a class="navbar-brand" href="<?= base_url('Employees'); ?>">
                    <img src="https://mdbootstrap.com/img/logo/mdb-transparent.png" height="30" alt="mdb logo">
                </a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i> <?= $this->session->userdata('username') ?> </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                                <a class="dropdown-item" href="<?= base_url('Employees/myProfile') ?>">My account</a>
                                <a class="dropdown-item" href="<?= base_url('Auth/logout'); ?>">Log out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
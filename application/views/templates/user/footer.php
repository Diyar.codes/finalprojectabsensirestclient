<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Diyar.codes 2020</span>
        </div>
    </div>
</footer>
</div>
</div>

<script type="text/javascript" src="<?= base_url('assets/MDB-Free.4.19.1/js/') ?>jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/MDB-Free.4.19.1/js/') ?>popper.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/MDB-Free.4.19.1/js/') ?>bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/MDB-Free.4.19.1/js/') ?>mdb.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="<?= base_url('assets/SB ADMIN2/'); ?>js/sb-admin-2.min.js"></script>
<script src="<?= base_url('assets/js/main.js'); ?>"></script>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('Employees'); ?>">
        <div class="sidebar-brand-icon">
            <i class="fas fa-building"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin Office</div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'index') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Employees/index') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'gajiBulanan') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Employees/gajiBulanan') ?>">
            <i class="fas fa-hand-holding-usd"></i>
            <span>Gaji Bulanan</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item <?php if ($this->uri->segment(2) == 'complain') {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="<?= base_url('Employees/complain') ?>">
            <i class="fas fa-compress-arrows-alt"></i>
            <span>Complain</span></a>
    </li>

    <hr class="sidebar-divider my-0">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>